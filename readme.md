# htpasswd_updater #

The htpasswd_updater leverages python to update .htpasswd files on remote machines.  Given an Excel spreadsheet and a remote machine spec file, the script will update the htpasswd file, email the users that were created and restart the webserver on the remote machine so that the changes will be picked up.

## Python Library Dependencies ##

1) paramiko: used for ssh and scp

2) scp: used for scp

3) SCPClient: used for scp

4) openpyxl: used for reading Excel spreadsheets

5) htpasswd: used for manipulating htpasswd files

6) email: used for email


##User account requirements##
You'll also need a username and key to access the machine you'll need to run automation against.  For example if you need access to the kibana logs machine, you'll need to have an ssh key added to the machine for you. You'll need to contact a member of the tfi-squad for this.

##Usage##
`python htpasswd_updater <remote_machine_spec.csv> <hipaa_workbook_from_Namrata.xlsx> [--commit] [--email=<log_recipient@email.com>]`

1) `remote_machine_spec.csv`: a csv file containing the ip address, username, keyfile, htpasswd file location and webserver restart command on the remote machine

2) `hipaa_workbook_from_Namrata.xlsx`: a Microsoft Excel workbook that is provided by Namrata Kumar that contains the users who are HIPAA certified

3) `--commit`: optionally specify whether changes should be committed to the remote. Omitting this option will cause the script to execute in "dry-run" mode

4) `--email=<log_recipient@email.com>`: optionally specify an email address to send the logs to. Logs will be saved in the bin folder of this project directory regardless of this option.

##Contents of the csv file##
The remote_machine_spec.csv file contains 4 columns, each describing a property that is required to perform the htpaswd update procedure: 

1) the ip address of the remote machine

2) the path to the key file on your local machine used to access the remote machine

3) the location of the htpasswd file on the remote 

4) the command to call to restart the webserver on the remote

##Contents of the xlsx file##
The Microsoft Excel spreadhseet that Namrata Kumar provides which contains the names and email addresses of users who have completed HIPAA training.  Usernames are derived from email addresses and passwords are automatically generated.  Users are emailed according to the email addresses entered in this spreadsheet.

This file can be downloaded from the [WA HIPAA Community wiki](https://w3-connections.ibm.com/communities/service/html/communityoverview?communityUuid=ce6e214a-97ff-4df8-9f7b-4e5491d93d0f).  If you can't access this wiki, you'll have to ask Namrata Kumar.

