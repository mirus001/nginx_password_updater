
from paramiko import SSHClient
from scp import SCPClient
from scp import SCPException
from htpasswd_generator import generate_htpasswd_file
from socket import error as socket_error

import emailer
import paramiko
import csv
import sys
import time
import os.path
import user
import shutil


# This script takes two inputs -- a csv containing the machines whose htpasswd files need to be updated and
#  an xlsx file, whose first worksheet contains the users to add to the htpasswd file
#
# The following columns will be read from the xlsx file:
#  1) Last Name
#  2) First Name
#  3) Username
#  4) Email

#The target machines csv contains the machines to apply the htpasswd file to and the destination folder
target_machines_csv = sys.argv[1]
#The new_htpasswd_file is the htpasswd file that contains the usernames and passwords to apply
hipaa_users_xlsx = sys.argv[2]
commit_changes = "no"
send_log_email = False
log_recipient = None
for entry in sys.argv:
    if "--email" in entry:
        send_log_email = True
        log_recipient = entry.split("=",1)[1]
        print (log_recipient)
    if "--commit" in entry:
        commit_changes = entry

timestamp_pattern = "%Y%m%d_%H%M%S"
work_dir = "bin"

if os.path.exists(work_dir):
    shutil.rmtree(work_dir)

os.makedirs(work_dir)

currentTime = time.strftime(timestamp_pattern)
original_copy = work_dir+"/.htpasswd_"+currentTime
new_htpasswd_file = work_dir+"/new_htpasswd.txt"

# Read target machine csv.  The columns are host_ip, username, password, htpasswd_full_path
f = open(target_machines_csv, 'rb') # opens the csv file
try:
    reader = csv.reader(f)  # creates the reader object
    for row in reader:   # iterates the rows of the file in orders
        host_ip = row[0]
        uname = row[1]
        passwd = row[2]
        htpasswd_full_path = row[3]
        update_command = row[4]
        print ("Attempting to update .htpasswd on "+host_ip)
        # attempt to connect via ssh to nginx box
        try:
            ssh = SSHClient()
            ssh.load_system_host_keys()
            print ("Attempting to connect to "+str(host_ip))
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            if(passwd.startswith('password=')):
                pwd_string = passwd.split("password=",1)[1]
                ssh.connect(host_ip, 22, username=uname, password=pwd_string)
            elif(passwd.startswith('keyfile=')):
                pkey_file = passwd.split("keyfile=",1)[1]
                key = paramiko.RSAKey.from_private_key_file(pkey_file)
                ssh.connect(host_ip, 22, username=uname, pkey=key)
            # attempt to retrieve the .htpasswd file
            try:
                scp = SCPClient(ssh.get_transport())
            
                #backup the original htpasswd file
                scp.get(htpasswd_full_path, original_copy)
                #we'll use the file as the baseline for the file to update
                shutil.copy(original_copy, new_htpasswd_file)
            except SCPException as scpException:
                print ("Could not retrieve existing htpasswd file, uploading for the first time")
            # attempt to replace the .htpasswod file
            try:
                scp = SCPClient(ssh.get_transport())

                users_to_email = generate_htpasswd_file(str(hipaa_users_xlsx), str(new_htpasswd_file))
                
                if(commit_changes == "--commit"):
                    for user in users_to_email:
                        print("user to email: "+user.username+" "+user.password+" "+user.email)

                    if len(users_to_email) > 0:
                        scp.put(new_htpasswd_file, htpasswd_full_path)

                        #also copy over the original copied previously
                        if(os.path.isfile(original_copy)):
                            scp.put(original_copy, htpasswd_full_path+"_"+currentTime)

                        print ("Executing nginx refresh")
                        stdin, stdout, stderr = ssh.exec_command(update_command)
                        exit_status = stdout.channel.recv_exit_status()          # Blocking call
                        if exit_status == 0:
                            print ("Nginx changes applied")
                        else:
                            print("*** Error: Could not complete nginx refresh!", exit_status)

            except IndexError:
                print ("Please specify a valid htpasswd file to upload as the 2nd parameter of this script")
            scp.close()
        except socket_error as s_err:
            print ("Could not connect to host "+host_ip+"... is the host up and reachable?")
finally:
    f.close()      # closing

if(commit_changes == "--commit"):
    print ("Committing changes to remote")
    for user in users_to_email:
        emailer.send_email_to_new_user(user_to_email=user)
        time.sleep(3)
else:
    print ("Not committing changes to remote. Specify --commit in the cli to apply changes.")

if os.path.exists(new_htpasswd_file) and send_log_email:
    emailer.send_email_with_file("nginx_lockdown_logs@bot.ibm.com", log_recipient, "Nginx-HIPAA-lockdown", ".htpasswd and logs from latest run", [new_htpasswd_file, "logfile"])

