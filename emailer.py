from email.header import Header
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
import user
import smtplib
import traceback

server = "na.relay.ibm.com"
def send_email(sender, to, message):
	smtpserver = smtplib.SMTP(server)
	smtpserver.ehlo()
	smtpserver.starttls()

	smtpserver.sendmail(sender, to, message)
	print ('done!')
	smtpserver.close()

def send_email_with_file(sender, to, subject, text, files=None):
	msg = MIMEMultipart(
		From=sender,
		To=COMMASPACE.join(to),
		Date=formatdate(localtime=True),
		Subject=subject
	)
	msg.attach(MIMEText(text))

	for f in files or []:
		with open(f, "rb") as fil:
			msg.attach(MIMEApplication(
				fil.read(),
				Content_Disposition='attachment; filename="%s"' % f,
				Name=f
			))

	smtp = smtplib.SMTP(server)
	smtp.sendmail(sender, to, msg.as_string())
	smtp.close()

def send_email_to_new_user(user_to_email, sender="do.not.reply@tfi.ibm.com"):
	to = user_to_email.email
	username = user_to_email.username
	password = user_to_email.password
	first_name = user_to_email.first_name
	last_name = user_to_email.last_name
	body = "Hello %s %s,\n\n"
	body += "As a HIPAA-certified member of the Watson Analytics team, a username and password has been generated for you to access the Kibana production logs located at http://9.24.205.183 (aka: wa-logs.ottawa.ibm.com)"
	body += "\n\nusername:%s\npassword:%s\n"
	body += "\nUse of the 'kibana' username to view production logs is no longer allowed."
	body += "\n\nFor more information about HIPAA and Watson Analytics, please see:\n"
	body += "http://wa-logs.ottawa.ibm.com/hipaa_how_to.html"
	body += "\n\nPlease note that this is an auto-generated email. If you are having difficulty accessing the logs with the provided credentials, please contact Mirus Lu or a member of the TFI team."
	body += "\n\nSigned,"
	body += "\n\nA bot on behalf of Mirus Lu, Namrata Kumar and the TFI team"


	message = MIMEText(body %(first_name, last_name, username, password))
	message['Subject'] = Header("Your Production WA Kibana Credentials", 'utf-8')
	message['From'] = sender
	message['To'] = to
	log_file = open('logfile','a')
	try:
		send_email(sender, to, message.as_string())
		print ("Successfully sent email to "+to)
		log_file.write("Successfully sent email to address "+to)
	
	except Exception as e:
		print ("Could not send email to address "+to)
		log_file.write("Could not send email to address "+to)
		log_file.write("Exception "+str(e))
		traceback.print_exc()

def send_reminder(user_to_email, sender="do.not.reply@tfi.ibm.com"):
	to = user_to_email.email
	username = user_to_email.username
	password = user_to_email.password
	body = "Hello,\n\n"
	body += "This is a friendly reminder that the Kibana production logs located at http://9.24.205.183 (aka: wa-logs.ottawa.ibm.com) are now only accessible to users who have completed HIPAA training. The kibana username can no longer be used to access the production logs."
	body += "\nHere are your credentials to access the production logs:\n"
	body += "\n\nusername:%s\npassword:%s\n"
	body += "\n\nFor more information about HIPAA and Watson Analytics, please see:\n"
	body += "http://wa-logs.ottawa.ibm.com/hipaa_how_to.html"
	body += "\n\nPlease note that this is an auto-generated email. If you are having difficulty accessing the logs with the provided credentials, please contact Mirus Lu or a member of the TFI team."
	body += "\n\nSigned,"
	body += "\n\nA bot on behalf of Mirus Lu, Namrata Kumar and the TFI team"


	message = MIMEText(body %(username, password))
	message['Subject'] = Header("Reminder: Your Production WA Kibana Credentials", 'utf-8')
	message['From'] = sender
	message['To'] = to
	print ("sending email to "+str(user_to_email.email))
	send_email(sender, to, message.as_string())
