#generate htpasswd file given xls file
from openpyxl import Workbook
from openpyxl import load_workbook
from user import User
import string
import random
import sys
import htpasswd
import os
import warnings 


warnings.filterwarnings("ignore")
master_worksheet_name = "Prod List From Ahmad"
special_chars = ""
#workbook_file = sys.argv[1]
#worksheet_name = sys.argv[2]
#htpasswd_file = sys.argv[3]
def __generate_unique_password(size=8, chars=string.ascii_uppercase + string.ascii_lowercase + "23456789" + special_chars):
	return ''.join(random.SystemRandom().choice(chars) for _ in range(size))

def __get_master_user_dict(workbook_file, worksheet_name):
	wb = load_workbook(filename = workbook_file)
	ws = wb.get_sheet_by_name(worksheet_name)
	row_count = ws.max_row

	index=2 
	stop = 1, row_count    # This will allow you to set a lower and upper limit
	#for index, row in enumerate(ws.iter_rows()):
	blank_row_found = False
	rows = ws.iter_rows()
	row_iterator = enumerate(rows)
	master_users = {}

	while((index <= row_count) and (not blank_row_found)):
		try:
			row_iterator.next()
			username = ws.cell(row=index,column=3).value
			email = ws.cell(row=index,column=4).value
			
			if(username is None and email is None):
				blank_row_found = True
			else:
				master_users[email] = username
			index = index+1
		except:
			blank_row_found = True

	return master_users

def generate_htpasswd_file(workbook_file, htpasswd_file):
	#create a new htpasswd file
	print ("workbook file "+workbook_file)
	wb = load_workbook(filename = workbook_file)
	ws = wb.get_sheet_by_name("WA HIPAA User List")
	row_count = ws.max_row

	if(not os.path.isfile(htpasswd_file)):
		open(htpasswd_file, 'w')

	log_file = open('logfile','a')

	hipaa_users = []
	master_dict = __get_master_user_dict(workbook_file, master_worksheet_name)

	with htpasswd.Basic(htpasswd_file, mode="crypt") as userdb:

		#loop rows and extract 
		#  username from column C
		#  email from column D
		#  insert password in column E
		index=2 
		stop = 1, row_count    # This will allow you to set a lower and upper limit
		#for index, row in enumerate(ws.iter_rows()):
		blank_row_found = False
		rows = ws.iter_rows()
		row_iterator = enumerate(rows)

		while((index <= row_count) and (not blank_row_found)):
			try:
				row_iterator.next()				
				username = ws.cell(row=index,column=3).value
				email = ws.cell(row=index,column=4).value
				first_name = ws.cell(row=index,column=2).value
				last_name = ws.cell(row=index,column=1).value
				password = __generate_unique_password()
				if '@' not in email:
					print ("!!! MALFORMED EMAIL IN XLS from Namrata !!! "+email)
					emailname = email.rsplit('.', 3)[0]+"@"
					emaildomain = '.'.join(email.rsplit('.', 3)[1:4])
					email = emailname+emaildomain
					print ("\t>>>Possible email address: "+email)
				print ("User email "+email)

				if username is None:
					"\tFound blank username for "+email
					try:
						username = master_dict[email]
					except:
						username = email.split("@")[0]
				
				if(username is None or email is None):
					blank_row_found = True
				else:
					#print "Found username: "+username
					try:
						userdb.add(username, password)
						log_file.write(username+","+password+","+email+"\n")
						print("+\tAdded User "+username)
						hipaa_users.append(User(username, password, email, first_name, last_name))
					except htpasswd.basic.UserExists as e:
						log_file.write(username+" already exists. Not updating.\n")
						print("\tUser "+username+" already exists")

				index = index+1
			except:
				blank_row_found = True

	return hipaa_users

#master_users = generate_htpasswd_file(sys.argv[1], sys.argv[2])
#for value in master_users:
#	print value.username+" "+value.email
